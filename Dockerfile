FROM ligo/software
LABEL name="gw-compare" \
      maintainer="James Alexander Clark <james.clark@ligo.org>" \
      url="https://git.ligo.org/james-clark/gw-compare"

RUN git clone https://github.com/vim/vim.git \
      && cd vim && make -j install


# lalsuite lalinference_o2 branch
RUN git clone https://git.ligo.org/lscsoft/lalsuite.git \
      && cd lalsuite \
      && git checkout lalinference_o2 \
      && ./00boot && ./configure --prefix=/opt/lalinference_o2 --disable-lalstochastic --enable-swig-python && make -j install \
      && cd .. && rm -r lalsuite


# gw-compare
RUN /bin/bash -c "source /opt/lalinference_o2/etc/lalsuiterc \
      && pip install git+https://git.ligo.org/james-clark/gw-compare.git"

ENTRYPOINT ["/bin/bash"]

