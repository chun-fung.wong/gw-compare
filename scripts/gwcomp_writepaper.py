#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Produce publication-quality comparison plots of reconstructed time-domain
waveforms from LALInference, BayesWave and cWB.  Also prints summary statistics
to stdout. 

Requires pickled data from gwcomp_driver.py
"""

import sys, os
import numpy as np
import cPickle as pickle
import pycbc.filter

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import argparse


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--pickled-bayeswave-results", metavar="BW_PICKLE",
            type=str, default=None, help="Pickled reconstruction data from\
                    gwcomp_process.py, using BayesWave median for point estimates",
                    required=True)
    parser.add_argument("--pickled-cwb-results", metavar="CWB_PICKLE", type=str,
            default=None, help="Pickled reconstruction data from\
                    gwcomp_process.py, using CWB maxL for point estimates",
                    required=False)
    parser.add_argument("--output-filename", default="GW170104", type=str,
            help="Identifier to attach to output files")
    parser.add_argument("--whiten-scale", default=2.290652e-22, type=float,
            help="Normalisation to preserve strain amplitude @ 200 Hz after\
                    whitening", metavar="WHITENORM")
    parser.add_argument("--wavelet-only", default=False,
            action="store_true", help="boolean to only plot wavelet result")

    parser.add_argument("--xlims", default=[0.425, 0.625],
            help="x-axis limits for main panel", type=two_floats)
    parser.add_argument("--xlims-inset", default=[0.55, 0.52],
            help="x-axis limits for inset panel", type=two_floats)
    parser.add_argument("--ylims", default=[-4, 4],
            help="y-axis limits for main panel", type=two_floats)
    parser.add_argument("--ylims-inset", default=[-2.0, 2.0],
            help="y-axis limits for inset panel", type=two_floats)

    parser.add_argument("--reconstruction-residuals", default=False,
            action="store_true", help="""
            boolean to produce insets showing the
            residuals between reconstruction methods""")

    parser.add_argument("--passband", default=(35.0, 350.0), nargs=2,
            type=float, 
            help="band-pass observed data at this frequency for plotting")
    parser.add_argument("--filter-order", default=8, type=int,
            help="low-pass filter order")

    parser.add_argument("--Lshift", default=2.9, type=float,
            help="ms by which to shift L1 back in combined data plots")


    opts = parser.parse_args()

    opts.Lshift *= 1e-3

    return opts

def two_floats(value):
    values = value.split()
    if len(values) != 2:
        raise argparse.ArgumentError
    values = map(float, values)
    return values

def bandpass(timeseries, passband=(35, 350), delta_t=1./2048):
    filtered=pycbc.filter.highpass_fir(pycbc.types.TimeSeries(timeseries,
        delta_t=delta_t), min(passband), order=8)
    filtered=pycbc.filter.lowpass_fir(filtered, max(passband), order=8)
    return filtered

# --------------------------------------------------------------------------------
#
# Input
#

options = parser()

ifos = ['H1', 'L1']

# Load data and create parameters
print "loading results"

# Load all variables from bw results dictionary
bw_pickled_data = pickle.load(open(options.pickled_bayeswave_results))
for key in bw_pickled_data.keys():
    exec "{key}=bw_pickled_data['{key}']".format(key=key)
bw_pe_overlaps = [float(val) for val in bw_pickled_data['pe_overlaps']]
bw_pe_net_overlap = float(bw_pickled_data['pe_net_overlap'])
bw_pe_residuals = [ val for val in bw_pickled_data['pe_residuals'] ]
bw_snrs = [ val for val in bw_pickled_data['burst_snrs'] ]
bw_net_snr = bw_pickled_data['burst_net_snr']
li_snrs = [ val for val in bw_pickled_data['li_snrs'] ]
li_net_snr = bw_pickled_data['li_net_snr']

if options.pickled_cwb_results is not None:
    # Load in the maxL point estimates from the run with CWB
    cwb_pickled_data = pickle.load(open(options.pickled_cwb_results))
    cwb_pe_overlaps = [float(val) for val in cwb_pickled_data['pe_overlaps']]
    cwb_pe_net_overlap = float(cwb_pickled_data['pe_net_overlap'])
    cwb_pe_residuals = [ val for val in cwb_pickled_data['pe_residuals'] ]
    cwb_waves = [ val for val in cwb_pickled_data['cwb_waves'] ]
    cwb_snrs = [ val for val in cwb_pickled_data['burst_snrs'] ]
    cwb_net_snr = cwb_pickled_data['burst_net_snr']
    cwb_multi_overlap_intervals = cwb_pickled_data['multi_overlap_intervals']
    net_cwb_multi_overlap_intervals = cwb_pickled_data['net_multi_overlap_intervals']
else:
    cwb_snrs = [np.nan for i in ifos]
    cwb_net_snr = np.nan
    cwb_pe_overlaps = [np.nan for i in ifos]
    cwb_pe_net_overlap=np.nan 
    cwb_multi_overlap_intervals = np.zeros(shape=(len(ifos),3)) + np.nan
    net_cwb_multi_overlap_intervals = np.zeros(3) + np.nan


# --------------------------------------------------------------------------------
#
# Print Results Summary
#

results_str="""
********************** RESULTS SUMMARY **********************

--- Point Estimates ---

LI_ML  = Maximum-likelihood LALInference waveform
CWB_ML  = Maximum-likelihood CWB waveform
BW_MED  = Median BayesWave waveform

* BayesWave SNR: (BW_MED | BW_MED)
    H1:{bw_snr_h:.2f} L1:{bw_snr_l:.2f}
    Network:{bw_net_snr:.2f}

* CWB SNR: (CWB_ML | CWB_ML)
    H1:{cwb_snr_h:.2f} L1:{cwb_snr_l:.2f}
    Network:{cwb_net_snr:.2f}

* LI SNR: (LI_ML | LI_ML)
    H1:{li_snr_h:.2f} L1:{li_snr_l:.2f}
    Network:{li_net_snr:.2f}

* CWB-LALInference Overlaps: (LI_ML | CWB_ML) 
    H1: {cwb_overlap_h:.2f} L1: {cwb_overlap_l:.2f}
    Network: {cwb_overlap_net:.2f}

* BayesWave-LALInference Overlaps (LI_ML | BW_MED) 
    H1: {bw_overlap_h:.2f} L1: {bw_overlap_l:.2f}
    Network: {bw_overlap_net:.2f} 

--- Results From Waveform Posterior Draws ---

* Overlap Posterior (LI | BW) Median +/- 90%-interval
    H1: {overlap_low90_h:.2f} {overlap_median_h:.2f} {overlap_upp90_h:.2f}
    L1: {overlap_low90_l:.2f} {overlap_median_l:.2f} {overlap_upp90_l:.2f}
    Network: {overlap_low90_net:.2f} {overlap_median_net:.2f} {overlap_upp90_net:.2f}

* Overlap Posterior (LI | BW_MED ) Median +/- 90%-interval
    H1: {multi_overlap_low90_h:.2f} {multi_overlap_median_h:.2f} {multi_overlap_upp90_h:.2f}
    L1: {multi_overlap_low90_l:.2f} {multi_overlap_median_l:.2f} {multi_overlap_upp90_l:.2f}
    Network: {multi_overlap_low90_net:.2f} {multi_overlap_median_net:.2f} {multi_overlap_upp90_net:.2f}

* Overlap Posterior (LI | CWB_ML) Median +/- 90%-interval
    H1: {cwb_multi_overlap_low90_h:.2f} {cwb_multi_overlap_median_h:.2f} {cwb_multi_overlap_upp90_h:.2f}
    L1: {cwb_multi_overlap_low90_l:.2f} {cwb_multi_overlap_median_l:.2f} {cwb_multi_overlap_upp90_l:.2f}
    Network: {cwb_multi_overlap_low90_net:.2f} {cwb_multi_overlap_median_net:.2f} {cwb_multi_overlap_upp90_net:.2f}

""".format(
        bw_snr_h=bw_snrs[0], bw_snr_l=bw_snrs[1],
        bw_net_snr=bw_net_snr,
        cwb_snr_h=cwb_snrs[0], cwb_snr_l=cwb_snrs[1],
        cwb_net_snr=cwb_net_snr,
        li_snr_h=li_snrs[0], li_snr_l=li_snrs[1],
        li_net_snr=li_net_snr,
        cwb_overlap_h=cwb_pe_overlaps[0], cwb_overlap_l=cwb_pe_overlaps[1],
        cwb_overlap_net=cwb_pe_net_overlap, 
        bw_overlap_h=bw_pe_overlaps[0], bw_overlap_l=bw_pe_overlaps[1],
        bw_overlap_net=bw_pe_net_overlap, 

        overlap_low90_h=overlap_intervals[0][0],
        overlap_median_h=overlap_intervals[0][1],
        overlap_upp90_h=overlap_intervals[0][2],
        overlap_low90_l=overlap_intervals[1][0],
        overlap_median_l=overlap_intervals[1][1],
        overlap_upp90_l=overlap_intervals[1][2],
        overlap_low90_net=net_overlap_intervals[0],
        overlap_median_net=net_overlap_intervals[1],
        overlap_upp90_net=net_overlap_intervals[2],

        multi_overlap_low90_h=multi_overlap_intervals[0][0],
        multi_overlap_median_h=multi_overlap_intervals[0][1],
        multi_overlap_upp90_h=multi_overlap_intervals[0][2],
        multi_overlap_low90_l=multi_overlap_intervals[1][0],
        multi_overlap_median_l=multi_overlap_intervals[1][1],
        multi_overlap_upp90_l=multi_overlap_intervals[1][2],
        multi_overlap_low90_net=net_multi_overlap_intervals[0],
        multi_overlap_median_net=net_multi_overlap_intervals[1],
        multi_overlap_upp90_net=net_multi_overlap_intervals[2],

        cwb_multi_overlap_low90_h=cwb_multi_overlap_intervals[0][0],
        cwb_multi_overlap_median_h=cwb_multi_overlap_intervals[0][1],
        cwb_multi_overlap_upp90_h=cwb_multi_overlap_intervals[0][2],
        cwb_multi_overlap_low90_l=cwb_multi_overlap_intervals[1][0],
        cwb_multi_overlap_median_l=cwb_multi_overlap_intervals[1][1],
        cwb_multi_overlap_upp90_l=cwb_multi_overlap_intervals[1][2],
        cwb_multi_overlap_low90_net=net_cwb_multi_overlap_intervals[0],
        cwb_multi_overlap_median_net=net_cwb_multi_overlap_intervals[1],
        cwb_multi_overlap_upp90_net=net_cwb_multi_overlap_intervals[2]
        )

print results_str
results_file = open(options.output_filename+'-results_summary.txt','w')
results_file.write(results_str)
results_file.close()

make_plots=False
if make_plots:

    # --------------------------------------------------------------------------------
    #
    # Plot Labels
    #
    print 'plotting'



    #
    # Plotting
    #

    # --- Time-domain Waveform Overlays
    #plt.style.use("ggplot")
    fig_width_pt = 246.0  # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0/72.27               # Convert pt to inches
    golden_mean = (np.sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height =fig_width*golden_mean       # height in inches
    fig_size = [fig_width,fig_height]
    params = {
              'axes.labelsize': 8,
              'font.size': 8,
              'legend.fontsize': 8,
              'xtick.color': 'k',
              'xtick.labelsize': 8,
              'ytick.color': 'k',
              'ytick.labelsize': 8,
              'text.usetex': True,
              'text.color': 'k',
              'figure.figsize': fig_size
              }
    import pylab
    pylab.rcParams.update(params)


    # Adjust times so that zero is on an integer second
    delta_t = np.diff(local_times)[0]
    trigtime,delta = divmod(opts.trigtime,1)
    local_times += delta
    import subprocess
    command = ['lalapps_tconvert', str(int(trigtime))]
    p = subprocess.Popen(command, stdout=subprocess.PIPE)
    timestr = p.stdout.read().replace('\n','')

    fdata, axdata = plt.subplots(nrows=1, figsize=fig_size)
    axdata2 = axdata.twinx()
    fshared, axshared = plt.subplots(nrows=2, figsize=(fig_size[0], 1.5*fig_size[1]),
            sharex=True)
    axshared2 = [axshared[i].twinx() for i in xrange(len(ifos))]

    for i in xrange(len(ifos)):

        filtered_data = bandpass(white_data[i], options.passband, delta_t=delta_t)

        # --- All time-series
        axshared[i].plot(local_times, filtered_data, color='gray',
               label=r'$\textrm{Data}$', alpha=0.7, linewidth=0.4, zorder=4)

        axshared[i].fill_between(local_times, bandpass(bw_intervals[i][0],
            options.passband, delta_t=delta_t),
            bandpass(bw_intervals[i][2], options.passband, delta_t=delta_t),
            alpha=0.7, label=r'$\textrm{Wavelets}$', zorder=2,
            color=u'#1f77b4', lw=0)

        if not options.wavelet_only:
            axshared[i].fill_between(local_times, bandpass(li_intervals[i][0],
                options.passband, delta_t=delta_t),
                bandpass(li_intervals[i][2], options.passband, delta_t=delta_t),
                alpha=0.7, label=r'$\textrm{BBH}$', zorder=3,
                color=u'#ff7f0e', lw=0)

    #    axshared[i].plot(local_times, cwb_waves[i], color='k', label='CWB ML',
    #            alpha=1.0, linewidth=0.5)

        # Dual y-axis for strain
        axshared2[i].set_ylim(options.ylims)

        strainticks = [ amp_ratio[i]*float(label) for label in  axshared[i].get_yticks() ]
        straintick_labels = ["$%.1f$"%(s*1e21) for s in strainticks]
        sigmatick_labels=[r"$%d$"%int(lab) for lab in axshared[i].get_yticks()]

        if i==1: straintick_labels[-1] = ""
        if i==1: sigmatick_labels[-1] = ""

        axshared[i].set_yticklabels(straintick_labels)
        axshared2[i].set_yticklabels(sigmatick_labels)

        axshared[i].minorticks_on()
        axshared2[i].minorticks_on()
        labelstr=r"$\textrm{%s Strain }[10^{-21}]$"%ifos[i]
        axshared[i].set_ylabel('%s'%labelstr, color='k')
        axshared2[i].set_ylabel(r'$\sigma_{\textrm{noise}}$', color='k')

        axshared[i].set_xlim(options.xlims)
        axshared[i].set_ylim(options.ylims)

        #axshared[i].yaxis.set_major_locator(MaxNLocator(prune='lower'))

        axshared[i].grid(linestyle='-', color='gray', alpha=0.1)
        for axis in ['top','bottom','left','right']:
              axshared[i].spines[axis].set_linewidth(0.25)
              axshared2[i].spines[axis].set_linewidth(0.25)

        #axshared[0].legend(loc='lower left', bbox_to_anchor=(0.0125, -0.1))
        axshared[0].legend(bbox_to_anchor=(0., .99, 1., .102), loc=3,
                           ncol=3, mode="expand", borderaxespad=0., frameon=False)
        axshared[1].set_xlabel(r'$\textrm{Time from %s [s]}$'%timestr, color='k')



        if not options.wavelet_only and options.reconstruction_residuals:
            # Inset: Residuals
            axins = inset_axes(axshared[i], width="50%", height="30%", loc=4, borderpad=1)
            axins.set_xticklabels([])


            axins.fill_between(local_times, residuals_intervals[i][0],
                    residuals_intervals[i][2], color='gray', alpha=0.5,
                    label=r'$\textrm{90\% C.I.}$')# CI')
         
            if options.pickled_cwb_results is not None:
                axins.plot(local_times, cwb_pe_residuals[i], color='k',
                        label=r'$\mathcal{L}_{\textrm{max}}$', linewidth=0.5)
            if i==1: axins.legend(loc='center left', bbox_to_anchor=(-0.9, 0.5))

            axins.minorticks_on()

            axins.set_xlim(options.xlims_inset)
            axins.set_ylim(options.ylims_inset)

            mark_inset(axshared[i], axins, loc1=2, loc2=4, fc="none", ec="0.5")

            plt.locator_params(axis='x',nbins=5)

            axins.grid(linestyle='-', color='gray', alpha=0.1)
            for axis in ['top','bottom','left','right']:
                  axins.spines[axis].set_linewidth(0.25)

        fshared.tight_layout()
        fshared.subplots_adjust(hspace=0)
        fshared.savefig('wf_reconstructions.png')
        fshared.savefig('wf_reconstructions.pdf')

