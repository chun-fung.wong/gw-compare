#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Collect and save raw and whitened NR waveforms and parameters.
"""

import sys, os
import numpy as np
import string
import glob
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from optparse import OptionParser

import pycbc.types
import lal,lalsimulation

import gw_reconstruct as gwr

from glue import pipeline
from glue.ligolw import ligolw
from glue.ligolw import utils as ligolw_utils
from glue.ligolw import lsctables
# define a content handler
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass
lsctables.use_in(LIGOLWContentHandler)

def update_progress(progress):
    print '\r\r[{0}] {1}%'.format('#'*(progress/2)+' '*(50-progress/2), progress),
    if progress == 100:
        print "\nDone"
    sys.stdout.flush()

def xml_to_chieff_q(xmlfile):
    """
    Compute chi_eff and mass ratio for each NR waveform
    """

    # Read xml
    xmldoc = ligolw_utils.load_filename(xmlfile, contenthandler =
            LIGOLWContentHandler, verbose = True)

    sim_inspiral_table = lsctables.SimInspiralTable.get_table(xmldoc)

    m1=float(sim_inspiral_table.get_column('mass1'))
    m2=float(sim_inspiral_table.get_column('mass2'))
    s1z=float(sim_inspiral_table.get_column('spin1z'))
    s2z=float(sim_inspiral_table.get_column('spin2z'))

    chieff=lalsimulation.SimIMRPhenomBComputeChi(m1 * lal.MSUN_SI,
            m2 * lal.MSUN_SI, s1z, s2z)
    mass_ratio = min(m1,m2)/max(m1,m2)

    # Compute chieff, q
    return chieff, mass_ratio


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = OptionParser(description=__doc__)
    parser.add_option("--bw-dir", type=str)
    parser.add_option("--nr-dir", type=str, default=None)
    parser.add_option("--srate", type=int, default=2048)
    parser.add_option("--epoch", type=float, default=1167559934.6)
    parser.add_option("--trigtime", type=float, default=1167559936.6)
    parser.add_option("--duration", type=float, default=4.0)
    parser.add_option("--make-plots", default=False, action="store_true")
    parser.add_option("--label", type=str, default="NRwaves")

    (opts,args) = parser.parse_args()

    return opts, args

opts, args = parser()

ifos = ['H1', 'L1']

# Load PSDs for whitening
psd_infile_fmt = os.path.join(opts.bw_dir, 'IFO{}_psd.dat')
infiles = [psd_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
psds = [gwr.psd.interp_from_txt(infile, flow=10) for infile in infiles]


#
# Begin Loop over NR waveforms
#

# Identify NR waves
#groups = glob.glob(os.path.join(opts.nr_dir, opts.nr_group_pattern))

waveformdirs = os.listdir(opts.nr_dir)
waveformdirs = [os.path.join(opts.nr_dir, wave) for wave in waveformdirs]

nr_waves = {}
for w,waveformdir in enumerate(waveformdirs):
    print "Processing {0} ({1} of {2})".format(
            waveformdir, w, len(waveformdirs))

    nr_files = [ os.path.join(waveformdir,wfile) for wfile in
            glob.glob(os.path.join(waveformdir, "[H,L]*nr_Sequence*")) ]

    # Get label
    label = os.path.basename(nr_files[0]).replace("H1_nr_Sequence-","")
    label = label.replace("_event_0.dat","")

    # Collect raw and whitened waveforms
    timeseries = []
    timeseries_white = []
    for n, nr_file in enumerate(nr_files):

        nr_data = np.loadtxt(nr_file)
        nr_srate = 1./np.diff(nr_data[:,0])[0]
        nr_times = nr_data[:,0]+1167559936.5991

        # Reduce to LI seglen
        idx=np.concatenate(np.argwhere( (nr_times>=opts.epoch) *
            (nr_times<=opts.epoch+opts.duration) ))
        nr_data = nr_data[idx,1]

        # Load into a pycbc.TimeSeries
        nr_data = pycbc.types.TimeSeries(nr_data, delta_t=1./nr_srate,
                epoch=opts.epoch)

        # Resample and Whiten
        timeseries.append(gwr.Strain(pycbc.filter.resample_to_delta_t(nr_data,
            1./opts.srate), white=False).to_timeseries())
        fftnorm = 1./opts.srate * np.sqrt(float(len(timeseries[0]))/2)
        timeseries_white.append(gwr.whiten_strain(
            fftnorm*timeseries[n],psds[n]).to_timeseries())

#       # Get mass ratio, chieff
#       maxptxml=glob.glob(os.path.join(os.path.dirname(nr_file), "maxpt*"))
#       chieff, mass_ratio = xml_to_chieff_q(maxptxml[0])
#
        # Add waveforms and params to dictionary: 
        # ( raw_waveforms, white_waveforms, (chieff, mass_ratio) )
        nr_waves[label] = ( [np.array(ts.data) for ts in timeseries],
                [np.array(ts.data) for ts in timeseries_white])
                #(chieff, mass_ratio))
 

import cPickle as pickle
outname=opts.label+".pickle"
pickle.dump(nr_waves, open(outname, "wb"))

print "NR waveforms now available in ", outname




