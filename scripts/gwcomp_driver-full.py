#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Perform the main processing for waveform reconstruction comparisons.

This script computes overlaps and residuals between burst reconstructions and
LALInference reconstructions.   LALInference reconstructions are obtained by
loading ascii files produced using gwcomp_reclal.py.

Optional data products include a pickle containing the main summary statistics
and data required for publication figures and results, produced using
gwcomp_writepaper.py

"""

import sys, os
import glob
import numpy as np

import argparse

import pycbc.types
import pycbc.filter
from pycbc.waveform import utils as wfutils

import lal
import lalsimulation as lalsim
import gw_reconstruct as gwr


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--li-waveforms", type=str, default=None)
    parser.add_argument("--cwb-waveforms", type=str, default=None)
    parser.add_argument("--li-summary-waveforms", type=str, default=None)
    parser.add_argument("--injections", default=False, action="store_true")
    parser.add_argument("--cwb-injections", default=False, action="store_true")
    parser.add_argument("--bw-dir", type=str, default=None, required=True)
    parser.add_argument("--srate", type=int, default=2048)
    parser.add_argument("--title", type=str, default=None)
    parser.add_argument("--bw-epoch", type=float, default=None)
    parser.add_argument("--li-epoch", type=float, default=None)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--nwaves", type=int, default=None)
    parser.add_argument("--flow", type=float, default=20.)
    parser.add_argument("--bw-amp-thresh", default=None, type=float)
    parser.add_argument("--window-inspiral", default=False, action="store_true")
    parser.add_argument("--taper-offset", default=0.01, type=float)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--output-filename", default=None, type=str, required=True)
    parser.add_argument("--save-for-publication", default=False, action="store_true")
    parser.add_argument("--nr-dir", default=None, type=str)

    opts = parser.parse_args()

    return opts

def suppress_low_amps(waves, reference_wave, amp_threshold=0.01, beta=0.25):
    """
    Apply a Tukey window to the waveform posterior draws in <waves> such that
    their power is suppressed at times where the reference waveform
    <reference_wave> drops below amp_threshold*max(reference_wave)
    """

    # Get indices for significant bw amplitude
    sigamps = np.where(abs(reference_wave) >
            amp_threshold*max(abs(reference_wave)))[0]
    sigamp_range = range(sigamps[0],sigamps[-1]) 

    window = np.zeros(len(reference_wave))
    window[sigamp_range] = lal.CreateTukeyREAL8Window(len(sigamp_range),
            beta).data.data

    # Apply and return window
    return np.array([window*wave for wave in waves]), window

def postmerger(wave_data, edgeidx=None, time_offset=0.01, delta_t=1./2048):
    """
    Reduce wave to the post-merger, as defined by the location in edgeidx and a
    time-offset
    """

    wave = pycbc.types.TimeSeries(wave_data, delta_t=delta_t)

    if edgeidx is None:
        amp_max, maxidx = wave.abs_max_loc()
        edgeidx = maxidx - int(time_offset*wave.sample_rate)

    wave.data[:edgeidx] = 0.0
    wave = wfutils.taper_timeseries(wave, tapermethod='TAPER_START')

    return wave.data

def maximise_overlaps(bw_waves, li_waves, f_low=20.0):

    max_overlaps = np.zeros(len(bw_waves))

    bw_ts = [pycbc.types.TimeSeries(bw_waves[w], delta_t=1./opts.srate,
        epoch=opts.bw_epoch) for w in xrange(len(bw_waves))]

    li_ts = [pycbc.types.TimeSeries(li_waves[w], delta_t=1./opts.srate,
        epoch=opts.li_epoch) for w in xrange(len(li_waves))]

    for b, bw in enumerate(bw_ts):
        print b

        max_overlaps[b]  = max([pycbc.filter.overlap(bw, li,
                    low_frequency_cutoff=f_low) for li in li_ts])

    return max_overlaps

def network_overlap(ts1_list, ts2_list, f_low=20.0):
    """
    Compute network overlap
    """
    overlap_sum=0.
    overlap_sum = sum([pycbc.filter.overlap(ts1, ts2,
        low_frequency_cutoff=f_low, normalized=False) for ts1,ts2 in
        zip(ts1_list,ts2_list)])
    norm1 = sum([pycbc.filter.overlap(ts1, ts2,
        low_frequency_cutoff=f_low, normalized=False) for ts1,ts2 in
        zip(ts1_list,ts1_list)])
    norm2 = sum([pycbc.filter.overlap(ts1, ts2,
        low_frequency_cutoff=f_low, normalized=False) for ts1,ts2 in
        zip(ts2_list,ts2_list)])

    net_overlap = overlap_sum / np.sqrt(norm1*norm2)

    return net_overlap

# --------------------------------------------------------------------------------
#
# Input
#

opts = parser()

ifos = ['H1', 'L1']

if opts.nr_dir is not None: nr_recovery=True
else: nr_recovery=False

# --------------------------------------------------------------------------------
#
# BayesWave (bw)
#
bw_infile_fmt = os.path.join(opts.bw_dir,
        'signal_recovered_whitened_waveform.dat.{}')
bw_infiles = [bw_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
print "Loading bayeswave"
bw_waves = np.array([np.loadtxt(infile) for infile in bw_infiles])
if opts.nwaves is not None:
    nwaves = opts.nwaves
    idx = np.random.randint(0,len(bw_waves),size=opts.nwaves)
    bw_waves = [b[idx,:] for b in bw_waves]
else:
    nwaves=np.shape(bw_waves)[1]

# Derived quantities and data
seglen = np.shape(bw_waves)[2]
duration = seglen/float(opts.srate)

# Set epochs
if opts.bw_epoch is None:
    opts.bw_epoch = opts.trigtime - 0.5*duration
if opts.li_epoch is None:
    opts.li_epoch = opts.trigtime - 0.5*duration

# Suppress times with low BW amplitude
if opts.bw_amp_thresh is not None:
    print "suppressing low bw amplitudes"
    for i in xrange(len(ifos)):
        bw_median = np.median(bw_waves[i], axis=0)
        bw_waves[i], _ = suppress_low_amps(bw_waves[i], bw_median,
                amp_threshold=opts.bw_amp_thresh)

# Window out inspiral (pre-peak power in BBH waveform)
if opts.window_inspiral:
    print "Tapering BW to post-merger"
    for i in xrange(len(ifos)):
        bw_waves[i] = np.array([postmerger(bw, time_offset=opts.taper_offset)
                for bw in bw_waves[i]])

# Get credible intervals for reconstructed T-domain waveforms
bw_intervals=[]
for i in xrange(len(ifos)):
    bw_intervals.append([pycbc.types.TimeSeries(bdata, delta_t=1./opts.srate,
        epoch=opts.bw_epoch) for bdata in np.percentile(bw_waves[i], [5, 50, 95],
            axis=0)])

# --------------------------------------------------------------------------------
#
# CWB reconstruction
#
if opts.cwb_waveforms is not None:
    cwb_waves = []
    for cwbfile in opts.cwb_waveforms.split(','):
        cwb_data = np.loadtxt(cwbfile)
        cwb_times = cwb_data[:,0]
        idx = (cwb_times>=opts.bw_epoch) * (cwb_times<opts.bw_epoch+duration) 
        cwb_ML = cwb_data[idx,1]
        cwb_median = cwb_data[idx,2]
        cwb_epoch = cwb_times[idx][0] 
        if opts.cwb_injections: cwb_epoch += 1./opts.srate #bug

        cwb_waves.append(pycbc.types.TimeSeries(cwb_ML, epoch=cwb_epoch,
            delta_t=1./opts.srate))

        #cwb_waves.append(pycbc.types.TimeSeries(cwb_median, epoch=cwb_epoch,
        #    delta_t=1./opts.srate))

# --------------------------------------------------------------------------------
#
# Whitened Strain data
#

white_data_fmt = os.path.join(opts.bw_dir, 'whitened_data.dat.{}')
infiles = [white_data_fmt.format(ifo) for ifo in xrange(len(ifos))]
white_data = [np.loadtxt(infile) for infile in infiles]


# --------------------------------------------------------------------------------
#
# LALInference (li)
#

if not opts.injections and not opts.cwb_injections and not nr_recovery:
    
    
    print "loading lalinference results"

    li_waves = np.load(opts.li_waveforms)

    summary_files = opts.li_summary_waveforms.split(',')
    f = open(summary_files[0],'r')
    header = f.readline().replace('#','').split()
    f.close()

    li_best_hts_white = [pycbc.types.TimeSeries(np.loadtxt(summary_file,
        usecols=[header.index('whitened_MAP')]), delta_t=1./opts.srate,
        epoch=opts.li_epoch) for summary_file in summary_files]

    # Suppress times with low BW amplitude
    if opts.bw_amp_thresh is not None:
        print "suppressing LI with low BW amplitudes"
        for i in xrange(len(ifos)):
            li_waves[i], window = suppress_low_amps(li_waves[i], bw_median,
                    amp_threshold=opts.bw_amp_thresh)
            li_best_hts_white[i].data *= window

    # Window out inspiral (pre-peak power in BBH waveform)
    if opts.window_inspiral:
        print "Tapering LI to post-merger"
        for i in xrange(len(ifos)):
            li_waves[i] = np.array([postmerger(li, time_offset=opts.taper_offset)
                    for li in li_waves[i]])
            li_best_hts_white[i].data = postmerger(li_best_hts_white[i].data)

    # Get credible intervals for reconstructed T-domain waveforms
    li_intervals=[]
    for i in xrange(len(ifos)):
        li_intervals.append([pycbc.types.TimeSeries(ldata, delta_t=1./opts.srate,
            epoch=opts.li_epoch) for ldata in np.percentile(li_waves[i], [5, 50, 95],
                axis=0)])

    #
    # Figures of Merit
    #

    print "computing overlaps of bayeswave with lalinference"

    # --- Overlaps

    if opts.cwb_waveforms is not None:
        # Point-estimate overlaps: (MAP BBH | ML cWB)
        pe_overlaps = [pycbc.filter.overlap(cwb_waves[i], li_best_hts_white[i],
            low_frequency_cutoff=opts.flow) for i in xrange(len(ifos))]

        pe_net_overlap = network_overlap(cwb_waves, li_best_hts_white,
                f_low=opts.flow)

    else:
        # Point-estimate overlaps: (MAP BBH | median BWB)
        pe_overlaps = [pycbc.filter.overlap(bw_intervals[i][1],
            li_best_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        bw_medians = [bw_intervals[i][1] for i in xrange(len(ifos))]
        pe_net_overlap = network_overlap(bw_medians, li_best_hts_white,
                f_low=opts.flow)



    # Overlap distribution from waveform draws
    overlaps = np.zeros(shape=(len(ifos), nwaves))
    net_overlaps = np.zeros(nwaves)
    for w in xrange(nwaves):

        bw_ts = [pycbc.types.TimeSeries(bw_waves[i,w], delta_t=1./opts.srate,
            epoch=opts.bw_epoch) for i in xrange(len(ifos))]

        li_ts = [pycbc.types.TimeSeries(li_waves[i,w], delta_t=1./opts.srate,
            epoch=opts.li_epoch) for i in xrange(len(ifos))]

        overlaps[:,w] = [pycbc.filter.overlap(bw_ts[i], li_ts[i],
            low_frequency_cutoff=opts.flow) for i in xrange(len(ifos))]

        net_overlaps[w] = network_overlap(bw_ts, li_ts, f_low=opts.flow)


    # --- Residuals
    
    # Point estimates: BBH - wavelets
    if opts.cwb_waveforms is not None:
        pe_residuals = [li_best.data-cwave.data for li_best, cwave in
                zip(li_best_hts_white, cwb_waves)]
    else:
        #pe_residuals = [li_interval[1].data-bw_interval[1].data for
        #        li_interval, bw_interval in zip(li_intervals, bw_intervals)]
        pe_residuals = [li_best.data-bw_interval[1].data for
                li_best, bw_interval in zip(li_best_hts_white, bw_intervals)]

    # Residual distribution from waveform draws
    residuals = li_waves - bw_waves

# --------------------------------------------------------------------------------
#
# rapidPE+NR
#

if nr_recovery:

    print "Loading rapidPE NR waveform data"


    globpattern = [os.path.join(opts.nr_dir,"{}*dat".format(ifo)) for ifo in
            ifos]
    print globpattern
    nr_files = [ glob.glob(globpat)[0] for globpat in globpattern ]

    nr_best_hts_white=[]
    for n,nr_file in enumerate(nr_files):

        nr_data = np.loadtxt(nr_file)
        nr_srate = 1./np.diff(nr_data[:,0])[0]
        nr_times = nr_data[:,0]+1167559936.5991

        # Reduce to BW seglen
        li_start = float(bw_intervals[0][1].start_time)
        li_end = float(bw_intervals[0][1].end_time)
        idx=np.concatenate(np.argwhere( (nr_times>=li_start) *
            (nr_times<=li_end) ))
        nr_data = nr_data[idx,1]

        # Load into a pycbc.TimeSeries and resample
        nr_data = pycbc.types.TimeSeries(nr_data, delta_t=1./nr_srate,
                epoch=li_start)

        # Load PSDs for whitening
        psd_infile_fmt = os.path.join(opts.bw_dir, 'IFO{}_psd.dat')
        infile = psd_infile_fmt.format(n)
        psd = gwr.psd.interp_from_txt(infile, flow=10)

        # Whiten
        fac = 1./opts.srate * np.sqrt(float(seglen)/2)
        nr_best_hts = gwr.Strain(pycbc.filter.resample_to_delta_t(nr_data,
            1./opts.srate), white=False)
        nr_best_hts_white.append(gwr.whiten_strain(fac*nr_best_hts,psd).to_timeseries())


    # --- Overlaps

    # Point-estimate overlaps (Injected waveform|median wavelet)
    if opts.cwb_waveforms is not None:
        pe_overlaps = [pycbc.filter.overlap(cwb_waves[i],
            nr_best_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]
        pe_net_overlap = network_overlap(cwb_waves, li_best_hts_white,
                f_low=opts.flow)
    else:
        pe_overlaps = [pycbc.filter.overlap(bw_intervals[i][1],
            nr_best_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        bw_medians = [bw_intervals[i][1] for i in xrange(len(ifos))]
        pe_net_overlap = network_overlap(bw_medians, li_best_hts_white,
                f_low=opts.flow)

    # Overlap distribution from waveform draws
    overlaps = np.zeros(shape=(len(ifos), nwaves))
    net_overlaps = np.zeros(nwaves)
    for w in xrange(nwaves):

        bw_ts = [pycbc.types.TimeSeries(bw_waves[i,w], delta_t=1./opts.srate,
            epoch=opts.bw_epoch) for i in xrange(len(ifos))]

        overlaps[:,w] = [pycbc.filter.overlap(bw_ts[i], nr_best_hts_white[i],
            low_frequency_cutoff=opts.flow) for i in xrange(len(ifos))]

        net_overlaps[w] = network_overlap(bw_ts, li_ts, f_low=opts.flow)

    # --- Residuals
    
    # Point estimates: BBH - wavelets
    if opts.cwb_waveforms is not None:
        pe_residuals = [ inj.data-cwave.data for inj, cwave in
                zip(nr_best_hts_white,cwb_waves) ]
    else:
        pe_residuals = [ nr.data-bw_interval[1].data for nr,bw_interval in
                zip(nr_best_hts_white,bw_intervals) ]

    # Residual distribution from waveform draws
    residuals = np.array([inj - bw for inj, bw in zip(nr_best_hts_white, bw_waves)])


# --------------------------------------------------------------------------------
#
# Injections
#

def parse_snr(filepath):
    f=open(filepath,'r')
    snr={}
    for lines in f.readlines():
        parts=lines.split(':')
        snr[parts[0]] = float(parts[1])
    return snr

if opts.injections or opts.cwb_injections:

    print "Loading injected waveforms and computing overlaps"
    if opts.cwb_injections:
        injection_files = [glob.glob(os.path.join(opts.bw_dir,
            "{ifo}-pycbchwinj*".format(ifo=ifo)))[0] for ifo in ifos]
        epochs = [os.path.basename(infile).split('-')[2].split('.')[0] for
                infile in injection_files]
        inj_hts_white = [ pycbc.types.TimeSeries(np.loadtxt(infile),
            epoch=epoch, delta_t=1./opts.srate) for infile,epoch in
            zip(injection_files,epochs) ]
    else:
        inj_infile_fmt = os.path.join(opts.bw_dir,
                'injected_whitened_waveform.dat.{}')
        injection_files = [inj_infile_fmt.format(ifo) for ifo in
                xrange(len(ifos))]
        inj_hts_white = [ pycbc.types.TimeSeries(np.loadtxt(infile),
            epoch=opts.li_epoch, delta_t=1./opts.srate) for infile in
            injection_files ]

    snr = parse_snr(os.path.join(opts.bw_dir,'snr.txt'))


    # --- Overlaps

    # Point-estimate overlaps (Injected waveform|median wavelet)
    if opts.cwb_waveforms is not None:

        pe_overlaps = [pycbc.filter.overlap(cwb_waves[i],
            inj_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        pe_net_overlap = network_overlap(cwb_waves, inj_hts_white,
                f_low=opts.flow)

    else:
        pe_overlaps = [pycbc.filter.overlap(bw_intervals[i][1],
            inj_hts_white[i], low_frequency_cutoff=opts.flow) for i in
            xrange(len(ifos))]

        bw_medians = [bw_intervals[i][1] for i in xrange(len(ifos))]
        pe_net_overlap = network_overlap(bw_medians, inj_hts_white,
                f_low=opts.flow)

    # Overlap distribution from waveform draws
    overlaps = np.zeros(shape=(len(ifos), nwaves))
    net_overlaps = np.zeros(nwaves)
    for w in xrange(nwaves):

        bw_ts = [pycbc.types.TimeSeries(bw_waves[i,w], delta_t=1./opts.srate,
            epoch=opts.bw_epoch) for i in xrange(len(ifos))]

        overlaps[:,w] = [pycbc.filter.overlap(bw_ts[i], inj_hts_white[i],
            low_frequency_cutoff=opts.flow) for i in xrange(len(ifos))]

        net_overlaps[w] = network_overlap(bw_ts, inj_hts_white, f_low=opts.flow)

    # --- Residuals
    
    # Point estimates: BBH - wavelets
    if opts.cwb_waveforms is not None:
        pe_residuals = [ inj.data-cwave.data for inj, cwave in
                zip(inj_hts_white,cwb_waves) ]
    else:
        pe_residuals = [ inj.data-bw_interval[1].data for inj,bw_interval in
                zip(inj_hts_white,bw_intervals) ]

    # Residual distribution from waveform draws
    residuals = np.array([inj - bw for inj, bw in zip(inj_hts_white, bw_waves)])
    

# -------------------------------------------------------------------------------
#
# Compute common (to real and injections) quantities and save summary statistics
#

# Overlaps
overlap_intervals = [np.percentile(overlap, [5, 50, 95]) for overlap in
    overlaps]


net_overlap_intervals = np.percentile(net_overlaps, [5, 50, 95])

# Residuals
residuals_intervals = [np.percentile(res, [5, 50, 95], axis=0) for res in residuals]

# Chisq (sum of squared-residuals
pe_chisq = [np.sum(pe_residual**2 ) for pe_residual in pe_residuals] 
chisq = np.zeros(shape=(len(ifos), nwaves))
for w in xrange(nwaves):
    chisq[:,w] = [ np.sum(residual**2 ) for residual in residuals[:,w,:] ]

chisq_intervals = [np.percentile(chisq[i],[5, 50, 95]) for i in
        xrange(len(ifos))]

print 'Saving stats'

header = "# point_estimate_overlap overlap_draws_median overlap_draws_low90 overlap_draws_upp90 "
header += "point_estimate_chisq chisq_draws_median chisq_draws_low90 chisq_draws_upp90\n"
for i in xrange(len(ifos)):

    fname = ifos[i]+'_'+opts.output_filename+'_stats.txt'

    if opts.bw_amp_thresh is not None:
        fname = fname.replace('.txt','_windowed.txt')
    if opts.window_inspiral:
        fname = fname.replace('.txt','_postmerger.txt')

    save_arr = np.array([pe_overlaps[i], overlap_intervals[i][1],
        overlap_intervals[i][0], overlap_intervals[i][2], pe_chisq[i],
        chisq_intervals[i][1], chisq_intervals[i][0], chisq_intervals[i][2]])
    f=open(fname,'w')
    f.writelines(header)
    result = [f.writelines('%f\t'%val) for val in save_arr.tolist()]
    f.writelines('\n')
    f.close()

# Network results
header = "# point_estimate_net_overlap net_overlap_draws_median net_overlap_draws_low90 net_overlap_draws_upp90 \n"

fname = 'NET_'+opts.output_filename+'_stats.txt'

if opts.bw_amp_thresh is not None:
    fname = fname.replace('.txt','_windowed.txt')
if opts.window_inspiral:
    fname = fname.replace('.txt','_postmerger.txt')

save_arr = np.array([pe_net_overlap, net_overlap_intervals[1],
    net_overlap_intervals[0], net_overlap_intervals[2]])

f=open(fname,'w')
f.writelines(header)
result = [f.writelines('%f\t'%val) for val in save_arr.tolist()]
f.writelines('\n')
f.close()

# -------------------------------------------------------------------------------


local_times = bw_intervals[i][1].sample_times-opts.trigtime

if opts.save_for_publication:
    print "pickling plot data"

    import cPickle as pickle

    # Convert ifo-lists of intervals lists to pickle-able format
    save_dict = {'ifos':ifos, 'opts':opts,
            'local_times':local_times,
            'white_data':white_data,
            'bw_intervals':[ [ts.data for ts in bw ] for bw in bw_intervals],
            'pe_residuals':pe_residuals,
            'residuals_intervals':residuals_intervals,
            'pe_overlaps':pe_overlaps,
            'pe_net_overlap':pe_net_overlap,
            'overlap_intervals':overlap_intervals,
            'net_overlap_intervals':net_overlap_intervals,
            'pe_chisq':pe_chisq,
            'chisq_intervals':chisq_intervals,
            'overlaps':overlaps,
            'net_overlaps':net_overlaps,
            'chisq':chisq
            }

    if opts.cwb_waveforms is not None:
        save_dict['cwb_waves']=[cwave.data for cwave in cwb_waves]

    if opts.injections:
        save_dict['inj_hts_white']=[inj.data for inj in inj_hts_white]
        save_dict['snr'] = snr
    elif nr_recovery:
        save_dict['nr_best_hts_white'] = [nr_best.data for nr_best in
                nr_best_hts_white]
    else: 
        save_dict['li_intervals'] = [ [li.data for li in li_interval] for
                li_interval in li_intervals ]
        save_dict['li_best_hts_white'] = [li_best.data for li_best in
                li_best_hts_white]

    fname = opts.output_filename+'_plotdata.pickle'
    pickle.dump(save_dict, open(fname, "wb"))

if opts.make_plots:

    import matplotlib
    matplotlib.use("Agg")
    from matplotlib import pyplot as plt
    from mpl_toolkits.axes_grid1.inset_locator import mark_inset
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes

    # Define median+upper_error-lower_error for labelling
    overlap_deltalow = [overlap_interval[1] - overlap_interval[0] for
            overlap_interval in overlap_intervals]
    overlap_deltahigh = [overlap_interval[2] - overlap_interval[1] for
            overlap_interval in overlap_intervals]
    overlap_medians = [overlap_interval[1] for overlap_interval in
            overlap_intervals]

    print 'plotting'

    #
    # Plotting
    #

    # --- Time-domain Waveform Overlays

    for i in xrange(len(ifos)):


        f, ax = plt.subplots(nrows=2,sharex=True,figsize=(10,8))
        f.subplots_adjust(hspace=0)

        ax[0].plot(local_times, white_data[i], color='gray', label='h(t)', alpha=0.5)

        if not opts.cwb_injections:
            ax[0].fill_between(local_times, bw_intervals[i][0], bw_intervals[i][2],
                    color='r', alpha=0.5, label='BayesWave 90% CI')

        if opts.cwb_waveforms is not None:
            ax[0].plot(local_times, cwb_waves[i], label='CWB ML', color='r')
        else:
            ax[0].plot(local_times, bw_intervals[i][1], label='BayesWave MED', color='r')

        if not opts.injections and not opts.cwb_injections and not nr_recovery:
            ax[0].fill_between(local_times, li_intervals[i][0], li_intervals[i][2], color='k',
                 alpha=0.5)
            ax[0].plot(local_times, li_best_hts_white[i], label='BBH Template 90% CI', color='k')
        elif opts.injections or opts.cwb_injections:
            ax[0].plot(local_times, inj_hts_white[i], label='BBH injection', color='k')

        if nr_recovery:
            ax[0].plot(local_times, nr_best_hts_white[i], label='BBH NR Sim MmargL', color='k')


        ax[0].minorticks_on()
        ax[0].grid(color='gray',linestyle='-')
        #ax[0].set_ylabel('Whitened h(t)')
        ax[0].set_xlim(-0.15, 0.05)
        ax[0].set_ylim(-5, 4)
        ax[0].legend(loc='upper left')

        #ax[0].set_xlabel('Time from %.2f (s)'%opts.trigtime)


        if not opts.injections and not opts.cwb_injections:
            titstr=r"Instrument: %s"%ifos[i]
            titstr += '\n'
            if opts.cwb_waveforms is not None:
                if nr_recovery:
                    titstr += r"Point-estimate: $(h_{\rm NR}^{\rm MmargL} | h_{\rm cwb}^{\rm ML})=%.2f$"%(pe_overlaps[i])
                else:
                    titstr += r"Point-estimate: $(h_{\rm BBH}^{\rm MAP} | h_{\rm cwb}^{\rm ML})=%.2f$"%(pe_overlaps[i])
            else:
                if nr_recovery:
                    titstr += r"Point-estimate: $(h_{\rm NR}^{\rm MmargL} | h_{\rm bw}^{\rm MED})=%.2f$"%(pe_overlaps[i])
                else:
                    titstr += r"Point-estimate: $(h_{\rm BBH}^{\rm MAP} | h_{\rm bw}^{\rm MED})=%.2f$"%(pe_overlaps[i])
            titstr += '\n'
            titstr+=r'Posterior draws: $(h_{\rm BBH} | h_{\rm bw})=%.2f^{+%.2f}_{-%.2f}$'%(
                    overlap_medians[i], overlap_deltahigh[i],
                    overlap_deltalow[i])
        else:
            titstr=r"GPS %.2f, Instrument: %s, Injected SNR: %.2f"%(opts.trigtime, ifos[i], snr[ifos[i]])
            titstr += '\n'
            if opts.cwb_waveforms is not None:
                titstr += r"Point-estimate: $(h_{\rm BBH}^{\rm INJ} | h_{\rm cwb}^{\rm ML})=%.2f$"%(pe_overlaps[i])
            else:
                titstr += r"Point-estimate: $(h_{\rm BBH}^{\rm INJ} | h_{\rm bw}^{\rm MED})=%.2f$"%(pe_overlaps[i])

        ax[0].set_title(titstr)

        axins = inset_axes(ax[0], width="40%", height="40%", loc=3, borderpad=2)
        axins.plot(local_times, white_data[i], color='gray', label='h(t)',
                alpha=0.5)
        if not opts.cwb_injections:
            axins.fill_between(local_times, bw_intervals[i][0],
                    bw_intervals[i][2], color='r', alpha=0.5)

        if opts.cwb_waveforms is not None:
            axins.plot(local_times, cwb_waves[i], label='Wavelets', color='r')
        else:
            axins.plot(local_times, bw_intervals[i][1], label='Wavelets', color='r')

        if not opts.injections and not opts.cwb_injections and not nr_recovery:
            axins.fill_between(local_times, li_intervals[i][0],
                    li_intervals[i][2], color='k', alpha=0.5)
            axins.plot(local_times, li_best_hts_white[i], label='BBH Template', color='k')
        elif opts.injections or opts.cwb_injections:
            axins.plot(local_times, inj_hts_white[i], label='BBH injection', color='k')

        if nr_recovery:
            axins.plot(local_times, nr_best_hts_white[i], label='BBH NR Sim MmargL', color='k')

        axins.minorticks_on()
        axins.set_xlim(-0.025, 0.03)
        axins.grid(color='gray')
        plt.locator_params(axis='x',nbins=5)

        #mark_inset(ax[0], axins, loc1=2, loc2=4, fc="none", ec="black", linewidth=1,
        #        linestyle=':')

        f.tight_layout()

    
        ax[1].plot(local_times, pe_residuals[i], color='k', linewidth=2,
                label='residuals from point-estimate waveforms')

        if not opts.cwb_injections:
            ax[1].fill_between(local_times, residuals_intervals[i][0],
                    residuals_intervals[i][2], color='gray', alpha=0.5,
                    label='90% CI from waveform posterior draws')


        ax[1].minorticks_on()
        ax[1].grid(color='gray',linestyle='-')
        ax[1].set_ylabel(r'$h_{\rm BBH} - h_{\rm wavelet}$')
        ax[1].set_xlim(-0.15, 0.05)
        ax[1].set_ylim(-4, 3)
        ax[1].legend(loc='upper left')

        ax[1].set_xlabel('Time from %.2f (s)'%opts.trigtime)

        if not opts.injections and not opts.cwb_injections:
            titstr=''
            if opts.cwb_waveforms is not None:
                if nr_recovery:
                    titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm MmargL} - h_{\rm cwb}^{\rm ML})^2=%.2f$"%(pe_chisq[i])
                else:
                    titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm MAP} - h_{\rm cwb}^{\rm ML})^2=%.2f$"%(pe_chisq[i])
            else:
                if nr_recovery:
                    titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm MmargL} - h_{\rm bw}^{\rm MED})^2=%.2f$"%(pe_chisq[i])
                else:
                    titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm MAP} - h_{\rm bw}^{\rm MED})^2=%.2f$"%(pe_chisq[i])
            titstr += '\n'
            titstr += r"Posterior draws: $\sum (h_{\rm BBH} - h_{\rm bw})^2=%.2f_{-%.2f}^{+%.2f}$"%(
                    chisq_intervals[i][1], chisq_intervals[i][0],
                    chisq_intervals[i][2],)
        elif opts.injections:
            titstr=''
            if opts.cwb_waveforms is not None:
                titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm INJ} - h_{\rm cwb}^{\rm ML})^2=%.2f$"%(pe_chisq[i])
            else:
                titstr += r"Point-estimate: $\sum (h_{\rm BBH}^{\rm INJ} - h_{\rm bw}^{\rm MED})^2=%.2f$"%(pe_chisq[i])


        ax[1].set_title(titstr)

        axins = inset_axes(ax[1], width="40%", height="40%", loc=3, borderpad=2)
        axins.plot(local_times, pe_residuals[i], color='k', linewidth=2)
        if not opts.cwb_injections:
            axins.fill_between(local_times, residuals_intervals[i][0],
                    residuals_intervals[i][2], color='gray', alpha=0.5)

        axins.grid(color='gray')

        axins.minorticks_on()
        axins.set_xlim(-0.025, 0.03)
        axins.set_ylim(-2.01,2.01)
        plt.locator_params(axis='x',nbins=5)

        #mark_inset(ax[1], axins, loc1=2, loc2=4, fc="none", ec="black", linewidth=1,
        #        linestyle=':')

        f.tight_layout()

        #fname = ifos[i]+'_'+opts.output_filename+'_residuals.png'
        fname = ifos[i]+'_'+opts.output_filename+'.png'
        if opts.bw_amp_thresh is not None:
            fname = fname.replace('.png','_windowed.png')
        if opts.window_inspiral:
            fname = fname.replace('.png','_postmerger.png')
        f.savefig(fname)

    # overlap histogram
    if opts.cwb_injections: sys.exit()

    for i in xrange(len(ifos)):
        f, ax = plt.subplots()

        ax.hist(overlaps[i], bins=50, normed=True, alpha=0.5, histtype='stepfilled')
        ax.set_ylabel('p(m|%s)'%ifos[i])

        titstr=r'%s posterior draws: $(h_{\rm BBH} | h_{\rm wavelet})=%.2f^{+%.2f}_{-%.2f}$'%(
                ifos[i], overlap_medians[i], overlap_deltahigh[i],
                overlap_deltalow[i])
        ax.set_title(titstr)

        ax.minorticks_on()
        ax.grid(color='gray',linestyle='-')

        ax.set_xlabel(r'overlap [ (a|b) ]')
        f.tight_layout()

        fname = ifos[i]+'_'+opts.output_filename+'_overlaphist.png'
        if opts.bw_amp_thresh is not None:
            fname = fname.replace('.png','_windowed.png')
        if opts.window_inspiral:
            fname = fname.replace('.png','_postmerger.png')
        f.savefig(fname)

    #   start = int(0.25*opts.srate)
    #   end = int(0.125*opts.srate)
    #   peak = np.argmax(abs(li_best_hts_white[0]))
    #   window_idx = range(peak-start, peak+end)
    #   window = np.zeros(seglen)
    #   window[window_idx] = lal.CreateTukeyREAL8Window(len(window_idx), 0.25).data.data
    #
    #   for i in xrange(len(ifos)):
    #       li_best_hts_white[i].data *= window
    #
    #       for w in xrange(np.shape(li_waves)[1]):
    #           li_waves[i][w] *= window
    #           bw_waves[i][w] *= window
    #

    #
    # Hilbert Spectra
    #

    #   bw_amp_tmp  = np.zeros(shape=(len(bw), len(bw[0])))
    #   bw_freq_tmp = np.zeros(shape=(len(bw), len(bw[0])))
    #   for i in xrange(len(bw_amp_tmp)):
    #       bw_amp_tmp[i,:], bw_freq_tmp[i,:] = \
    #               HHT.HHT(bw[i].sample_times.data, bw[i].data, N=0)
    #
    #   bw_amp = np.percentile(bw_amp_tmp, [5, 50, 95], axis=0)
    #   bw_freq = np.percentile(bw_freq_tmp, [5, 50, 95], axis=0)
    #
    #
    #   li_amp_tmp  = np.zeros(shape=(len(li), len(li[0])))
    #   li_freq_tmp = np.zeros(shape=(len(li), len(li[0])))
    #   for i in xrange(len(li_amp_tmp)):
    #       li_amp_tmp[i,:], li_freq_tmp[i,:] = \
    #               HHT.HHT(li[i].sample_times.data, li[i].data, N=0)
    #
    #   li_amp = np.percentile(li_amp_tmp, [5, 50, 95], axis=0)
    #   li_freq = np.percentile(li_freq_tmp, [5, 50, 95], axis=0)

    # --- Instantaneous frequencies
    #   f, ax = plt.subplots()
    #
    #
    #   # HHT
    #   ax.fill_between(bw[0].sample_times-opts.trigtime, bw_freq[0], bw_freq[2],
    #           color='r', alpha=0.5)
    #   ax.step(bw[0].sample_times-opts.trigtime, bw_freq[1], label='Wavelets (HHT)', color='r')
    #
    #   # Zero-crossing f(t):
    #   ax.fill_between(bw[0].sample_times-opts.trigtime, bw_tftracks[:,4],
    #           bw_tftracks[:,5], color='k', alpha=0.5)
    #   ax.step(bw[0].sample_times-opts.trigtime, bw_tftracks[:,1], 
    #           label='Wavelets (zero-crossings)', color='k')
    #
    #   ax.set_ylim(0,350)
    #   ax.minorticks_on()
    #   ax.grid(color='gray',linestyle='-')
    #   ax.legend(loc='upper left')
    #   ax.set_xlabel('Time from %.2f (s)'%opts.trigtime)
    #   ax.set_ylabel('Instantaneous Frequency (Hz)')
    #   ax.set_xlim(-0.1, 0.02)
    #
    #   f.subplots_adjust(hspace=0)
    #   f.suptitle('Data: %s'%opts.title)
    #
    #
    #   plt.show()
