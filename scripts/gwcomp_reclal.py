#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#               2017-2020 Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Reconstructs LALInference waveforms.

Make waveforms using samples drawn from the posterior distribution on BBH
approximant parameters from LALInference.  This seems to work a lot better with
posterior samples from lalinference_nest.
"""

import sys, os
import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

import argparse

import pycbc.types
import pycbc.filter

import lalsimulation as lalsim
import gw_reconstruct as gwr
import glob

def update_progress(progress):
    print '\r\r[{0}] {1}%'.format('#'*(progress/2)+' '*(50-progress/2), progress),
    if progress == 100:
        print "\nDone"
    sys.stdout.flush()

def get_loglike(infile):
    """
    Extract the log-posterior series of the chain.
    """
    with open(infile, 'r') as inp:
        header = gwr.posterior.parse_header(inp)
        like_col = header.index('logl')
        loglike = np.genfromtxt(inp, usecols=(like_col), unpack=True)
    return loglike


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--bw-dir", type=str, default=None)
    parser.add_argument("--psd", type=str, nargs='*',default=None)
    parser.add_argument("--is-asd-file", default=False, action="store_true")
    parser.add_argument("--li-samples-file", type=str)
    parser.add_argument("--srate", type=float, default=2048)
    parser.add_argument("--li-epoch", type=float, default=1167559934.6)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--nwaves", type=int, default=200)
    parser.add_argument("--approx", type=str, default='IMRPhenomPv2')
    parser.add_argument("--duration", type=float, default=4.0)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--output-dir", default="./")
    parser.add_argument("--reference-frequency", default=200.0, type=float)
    parser.add_argument("--flow", default=20.0, type=float)
    parser.add_argument("--ifos", type=str, nargs='*', default=['H1', 'L1'])
    parser.add_argument("--choose-fd", default=False, action="store_true")
    parser.add_argument("--calibrate", default=False, action="store_true")

    args = parser.parse_args()

    return args

args = parser()
lifile = args.li_samples_file.split('/')[-1]

ifos = args.ifos
seglen = int(args.srate*args.duration)

if args.psd is not None :
  if(len(args.psd)!=len(ifos)):
    print "Please provide as many number of PSDs as number of detectors. %d v/s %d" %(len(args.psd),len(ifos) )
    sys.exit(1)
  infiles = args.psd

# Check if the PSD files have older or newer naming convention
if args.bw_dir is not None and args.psd is None:
  if(os.path.exists(os.path.join(args.bw_dir, 'IFO0_psd.dat'))):
    psd_infile_fmt = os.path.join(args.bw_dir, 'IFO{}_psd.dat')
    infiles = [psd_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
  else:
    if(os.path.exists(os.path.join(args.bw_dir, 'H1_fairdraw_psd.dat'))):
      psd_infile_fmt = os.path.join(args.bw_dir, '{}_fairdraw_psd.dat') # New naming format. Also needs storing of median PSD at the location
    elif(os.path.exists(os.path.join(args.bw_dir, 'post/clean/glitch_median_PSD_forLI_H1.dat'))):
      psd_infile_fmt = os.path.join(args.bw_dir, 'post/clean/glitch_median_PSD_forLI_{}.dat')
    else:
      print "Median PSD file not found. Using <ifo>_psd.dat file"
      psd_infile_fmt = os.path.join(args.bw_dir, '{}_psd.dat')
    infiles = [psd_infile_fmt.format(ifos[ifo]) for ifo in xrange(len(ifos))]

# Load PSDs for whitening
psds = [gwr.psd.interp_from_txt(infile, flow=16, asd_file=args.is_asd_file) for infile in infiles]

# Generate MAP waveform
li_map_sample = gwr.posterior.extract_map_sample(args.li_samples_file)

exec "approx=lalsim."+args.approx
li_map_hfs = gwr.generate_strain_from_sample(li_map_sample,
        duration=args.duration, epoch=args.li_epoch, sample_rate=args.srate, ifos=ifos,choose_fd=args.choose_fd, flow=args.flow, calibrate=args.calibrate,
        approx=approx)
li_map_hts = [h.tseries for h in li_map_hfs]

li_map_hfs_white = [gwr.whiten_strain(h, psd) for h, psd in zip(li_map_hfs, psds)]
li_map_hts_white = [h.tseries for h in li_map_hfs_white]

# XXX: Get normalization for amp @ 200 Hz
amp_ratio=np.zeros(len(ifos))
fftnorm = (1./args.srate) * np.sqrt(len(li_map_hts_white[0])/2)
taridx = np.argmin(abs(li_map_hfs[0].sample_frequencies-200))
amp_ratio[0]=np.abs(li_map_hfs[0][taridx])/np.abs(li_map_hfs_white[0][taridx])/fftnorm
#amp_ratio[0] = np.abs(li_map_hts[0].data[taridx])/np.abs(li_map_hts_white[0].data[taridx])
print """
Multiply y-ticks on whitened H1 waveforms by this ratio to show physical
amplitude @ reference frequency:
"""
print "AMP RATIO: ", amp_ratio

fftnorm = (1./args.srate) * np.sqrt(len(li_map_hts_white[1])/2)
taridx = np.argmin(abs(li_map_hfs[1].sample_frequencies-200))
amp_ratio[1]=np.abs(li_map_hfs[1][taridx])/np.abs(li_map_hfs_white[1][taridx])/fftnorm
#amp_ratio[1] = np.abs(li_map_hts[1].data[taridx])/np.abs(li_map_hts_white[1].data[taridx])
print """
Multiply y-ticks on whitened L1 waveforms by this ratio to show physical
amplitude @ reference frequency:
"""
print "AMP RATIO: ", amp_ratio


if(len(ifos)==3):
  fftnorm = (1./args.srate) * np.sqrt(len(li_map_hts_white[2])/2)
  taridx = np.argmin(abs(li_map_hfs[2].sample_frequencies-200))
  amp_ratio[2]=np.abs(li_map_hfs[2][taridx])/np.abs(li_map_hfs_white[2][taridx])/fftnorm
  #amp_ratio[2] = np.abs(li_map_hts[2].data[taridx])/np.abs(li_map_hts_white[2].data[taridx])
  print """
  #Multiply y-ticks on whitened V1 waveforms by this ratio to show physical
  #amplitude @ reference frequency:
  #"""
  print "AMP RATIO: ", amp_ratio


# Generate ML waveform
li_samples = gwr.extract_samples(args.li_samples_file)
loglikelihoods = get_loglike(args.li_samples_file)
li_ml_sample = li_samples[np.argmax(loglikelihoods)]

li_ml_hfs = gwr.generate_strain_from_sample(li_ml_sample,
        duration=args.duration, epoch=args.li_epoch, sample_rate=args.srate, ifos=ifos, choose_fd=args.choose_fd, flow=args.flow, calibrate=args.calibrate, approx=approx)
li_ml_hts = [h.tseries for h in li_ml_hfs]

li_ml_freqevo = gwr.freq_evolution_from_sample(li_ml_sample,
        duration=args.duration, epoch=args.li_epoch, sample_rate=args.srate, ifos=ifos, flow=args.flow, approx=approx)

li_ml_hfs_white = [gwr.whiten_strain(h, psd) for h, psd in zip(li_ml_hfs, psds)]
li_ml_hts_white = [h.tseries for h in li_ml_hfs_white]


# Generate random selection of sampled waveforms
idx = np.random.randint(0,len(li_samples),size=args.nwaves)

#Delete below line and change it to above line after debugging
#idx = np.arange(args.nwaves)

li_samples = li_samples[idx]

print "Generating lalinference waveforms"
li_waves = np.zeros(shape=(len(ifos), len(li_samples), seglen))
li_waves_raw = np.zeros(shape=(len(ifos), len(li_samples), seglen))
optimal_pe_snrs = np.zeros(len(li_samples))

######### Debug #########
#print "Printing the subset of posterior samples"
#types = li_samples.dtype.names
#header = " ".join(types)
#np.savetxt('posterior_samples_subset.dat', li_samples, header=header)

approx_names = {}
approx_names[lalsim.IMRPhenomPv2] = "IMRPhenomPv2"
approx_names[lalsim.SEOBNRv3] = "SEOBNRv3"

imrcounter=0
seobcounter=0
eobcounter=0
imrpdcounter=0
for s,sample in enumerate(li_samples):

    update_progress((s+1)*100/len(li_samples))
    
#    print "Generating ", approx_names[
#            gwr.sample_to_waveform_params(sample)['approx']
#            ]
    if gwr.sample_to_waveform_params(sample)['approx'] == lalsim.IMRPhenomPv2:
        imrcounter+=1
    if gwr.sample_to_waveform_params(sample)['approx'] == lalsim.SEOBNRv3:
        seobcounter+=1
    if gwr.sample_to_waveform_params(sample)['approx'] == lalsim.EOBNRv2:
        eobcounter+=1
    if gwr.sample_to_waveform_params(sample)['approx'] == lalsim.IMRPhenomD:
        imrpdcounter+=1

    hfs = gwr.generate_strain_from_sample(sample, duration=args.duration,
            epoch=args.li_epoch, sample_rate=args.srate, ifos=ifos, choose_fd=args.choose_fd, flow=args.flow, calibrate=args.calibrate, approx=approx)
            #epoch=args.li_epoch, sample_rate=args.srate, approx=approx)

    for i in xrange(len(ifos)):
        hts = hfs[i].tseries
        li_waves_raw[i,s,:] = hts.data

        hfs_white = gwr.whiten_strain(hfs[i], psds[i])
        hts_white = hfs_white.tseries
        li_waves[i,s,:] = hts_white.data
    optimal_pe_snrs[s] = sample['optimal_snr']

print "{} waveforms generated:".format(len(li_samples))
print "{} are IMRPhenomPv2".format(imrcounter)
print "{} are SEOBNRv3".format(seobcounter)
if(eobcounter>0):
    print '{} waveform are EOBNRv2'.format(eobcounter)
if(imrpdcounter>0):
    print '{} waveform are IMRPhenomD'.format(imrpdcounter)
#
# Get intervals for reconstructed T-domain waveforms
#
li_intervals=[]
li_intervals_raw=[]
for i in xrange(len(ifos)):

    li_intervals.append([pycbc.types.TimeSeries(ldata, delta_t=1./args.srate,
        epoch=args.li_epoch) for ldata in np.percentile(li_waves[i], [5, 50, 95],
            axis=0)])
    li_intervals_raw.append([pycbc.types.TimeSeries(ldata, delta_t=1./args.srate,
        epoch=args.li_epoch) for ldata in np.percentile(li_waves_raw[i], [5, 50, 95],
            axis=0)])

li_times = np.arange(args.trigtime -args.duration/2.0, args.trigtime + args.duration/2.0, 1./args.srate)
#
# Dump to file
#
print "Saving output"
for i in xrange(len(ifos)):

    fname = os.path.join(args.output_dir,
            ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('posterior','summary_waveforms'))

    header = "Time whitened_ML whitened_lower_bound_90 whitened_MAP whitened_median whitened_upper_bound_90 ML lower_bound_90 MAP median upper_bound_90 amp_ratio ML_freq"
    #np.savetxt(fname, np.array([li_map_hts_white[0].sample_times.data,
    np.savetxt(fname, np.array([li_times,
        li_ml_hts_white[i].data,
        li_intervals[i][0].data,
        li_map_hts_white[i].data,
        li_intervals[i][1].data,
        li_intervals[i][2].data,
        li_ml_hts[i].data,
        li_intervals_raw[i][0].data,
        li_map_hts[i].data,
        li_intervals_raw[i][1].data,
        li_intervals_raw[i][2].data,
        amp_ratio[i]*np.ones(len(li_map_hts[0])),
        li_ml_freqevo[i]
        ]).T,
        fmt="%.9f",
        header=header)

    fname = os.path.join(args.output_dir, 
            ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('posterior','waveforms'))
    np.savetxt(fname, li_waves[i].T)

# Sampled waveforms
fname = os.path.join(args.output_dir,
        args.li_samples_file.split('/')[-1].replace('posterior','waveforms'))
np.save(fname, li_waves)

fname = os.path.join(args.output_dir,
        args.li_samples_file.split('/')[-1].replace('posterior','raw_waveforms'))
np.save(fname, li_waves_raw)

# Optimal SNRs
fname = os.path.join(args.output_dir,
        args.li_samples_file.split('/')[-1].replace('posterior','optimal_pe_snr'))
np.save(fname, optimal_pe_snrs)

if args.make_plots:
    # --- Time-domain Overlays
    li_times = li_times - args.trigtime

    for i in xrange(len(ifos)):
        # Whitened full
        f, ax = plt.subplots()

        ax.fill_between(li_times, li_intervals[i][0], li_intervals[i][2], color='k',
             alpha=0.25)
        ax.plot(li_times, li_map_hts_white[i], label='MAP', color='k')
        ax.plot(li_times, li_ml_hts_white[i], label='ML', color='k', linestyle='--')
        ax.minorticks_on()
        ax.grid(color='gray',linestyle='-')
        ax.set_ylabel('%s whitened h(t)'%ifos[i])
        ax.legend(loc='upper left')

        ax.set_xlabel('Time from %.2f (s)'%args.trigtime)

        f.tight_layout()

        fname = os.path.join(args.output_dir,
                ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('.dat','-liwaveforms_full.png'))
        plt.savefig(fname)
        ax.set_xlim(-0.1, 0.1)
        fname = os.path.join(args.output_dir,
                ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('.dat','-liwaveforms.png'))
        plt.savefig(fname)
        plt.close('all')
        # De-whitened full
        f, ax = plt.subplots()

        ax.fill_between(li_times, li_intervals_raw[i][0], li_intervals_raw[i][2], color='k',
             alpha=0.25)
        ax.plot(li_times, li_map_hts[i], label='MAP', color='k')
        ax.plot(li_times, li_ml_hts[i], label='ML', color='k', linestyle='--')
        ax.minorticks_on()
        ax.grid(color='gray',linestyle='-')
        ax.set_ylabel('%s h(t)'%ifos[i])
        ax.legend(loc='upper left')

        ax.set_xlabel('Time from %.2f (s)'%args.trigtime)

        f.tight_layout()

        fname = os.path.join(args.output_dir,
                ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('.dat','-liwaveforms_raw_full.png'))
        plt.savefig(fname)
        ax.set_xlim(-0.1, 0.1)
        fname = os.path.join(args.output_dir,
                ifos[i]+'_'+args.li_samples_file.split('/')[-1].replace('.dat','-liwaveforms_raw.png'))
        plt.savefig(fname)
        plt.close('all')
