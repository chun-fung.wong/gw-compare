#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
Find the time and frequency at which  median BayesWave reconstructions reach
SNRSQ_THRESHOLD fraction of their total SNR^2
"""

import sys, os
import numpy as np
from scipy import signal

import argparse

import lal
from pycbc import types
from pycbc import filter

def compute_snr(signaldata, delta_t=1./2048, psd_ratio=1.0, f_min=16.0):
    fftnorm = np.sqrt(psd_ratio)*delta_t * np.sqrt(len(signaldata)/2)
    tdomain_signal = types.TimeSeries(signaldata, delta_t=delta_t)
    fdomain_signal = tdomain_signal.to_frequencyseries()/fftnorm
    return filter.sigma(fdomain_signal, low_frequency_cutoff=f_min)


def window_data(wavedata, windowlen, beta=0.01):
    """
    Apply a Tukey window of len windowlen to the START/END of the data in wavedata
    """

    window = np.zeros(len(wavedata))
    window[:windowlen] = lal.CreateTukeyREAL8Window(windowlen,
            beta).data.data

    # Apply and return window
    return window*wavedata, window

def resample_to_delta_t(signaldata, delta_t, new_delta_t):
    seglen = len(signaldata)
    new_seglen = int(seglen * delta_t/new_delta_t)
    return signal.resample(signaldata,new_seglen)


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--bw-path", type=str, default="./")
    parser.add_argument("--waveform", type=str,
            default="signal_median_time_domain_waveform.dat")
    parser.add_argument("--tftrack", type=str,
            default="signal_median_tf.dat")
    parser.add_argument("--srate", type=int, default=2048)
    parser.add_argument("--epoch", type=float, default=1167559934.6)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--duration", type=float, default=4.0)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--snrsq-threshold", default=0.05, type=float)
    parser.add_argument("--f-min", default=16.0, type=float)
    parser.add_argument("--window-size", default=0.0125, type=float)
    parser.add_argument("--new-srate", default=None)

    opts = parser.parse_args()

    return opts


opts = parser()

ifos = ['H1', 'L1']

#
# Load Data
#
bw_signals = [np.loadtxt( os.path.join(opts.bw_path,
    opts.waveform+".{}".format(i)))[:,1] for i in xrange(len(ifos))]
    
bw_tftracks = [np.loadtxt( os.path.join(opts.bw_path,
    opts.tftrack+".{}".format(i)))[:,1] for i in xrange(len(ifos))]

if opts.new_srate is not None:
    print "resampling"
    bw_signals = [resample_to_delta_t(bw, 1./opts.srate, 1./opts.new_srate) for bw in
            bw_signals]
    bw_tftracks = [resample_to_delta_t(bw, 1./opts.srate, 1./opts.new_srate) for bw in
            bw_tftracks]
else: opts.new_srate = opts.srate

# Derived quantities
delta_t = 1./opts.new_srate
seglen = opts.duration/delta_t
psd_ratio = 1./opts.srate / (1./opts.new_srate)
times = np.arange(opts.epoch, opts.epoch+opts.duration, delta_t)


# Compute full SNRs
full_snr = [compute_snr(bw, delta_t=delta_t, f_min=opts.f_min,
    psd_ratio=psd_ratio) for bw in
        bw_signals]
full_net_snrsq = np.sum(np.square(full_snr))

print "H1 SNR:{0}, L1 SNR:{1}".format(full_snr[0],full_snr[1])

# Apply windowing to determine where SNR^2 gets to threshold
windowlen = int(opts.window_size*opts.srate)
nwindows = int(seglen / windowlen)
accumulated_snr = np.zeros(shape=(len(ifos),nwindows))
w=1
for i in xrange(int(nwindows)):
#    print "Finding SNR in chunk {0} of {1}".format(w,nwindows+1)

    for j in xrange(len(ifos)):
        windowed_data, window = window_data(bw_signals[j], windowlen=w*windowlen)
        accumulated_snr[j,i] = compute_snr(windowed_data, delta_t=delta_t,
                f_min=opts.f_min, psd_ratio=psd_ratio)

    w+=1

# Find the time closest to reaching snrsq-fraction threshold
accumulated_net_snrsq = np.sum(np.square(accumulated_snr),axis=0)
snrsq_fraction = accumulated_net_snrsq / full_net_snrsq
chunktimes = np.arange(1,nwindows+1)*(windowlen*delta_t) + opts.epoch

time_to_thresh = chunktimes[
        np.argmin(abs(snrsq_fraction-opts.snrsq_threshold))
        ]
print snrsq_fraction[
        np.argmin(abs(snrsq_fraction-opts.snrsq_threshold))
        ]

# Find the instantaneous frequencies closest to this time
idx = np.argmin(abs(times-time_to_thresh))

freqs_at_thresh = [bw_tftracks[i][idx] for i in xrange(len(ifos))]

print freqs_at_thresh

if opts.make_plots:
    print "making plots"
    import matplotlib
#    matplotlib.use("Agg")
    from matplotlib import pyplot as plt

    local_times = times - time_to_thresh
    local_chunktimes = chunktimes - time_to_thresh
    f, ax = plt.subplots(nrows=2,figsize=(10,6))

    for i in xrange(len(ifos)):
        ax[i].plot(local_times, bw_signals[i], label='strain')
        ax_snr = ax[i].twinx()
        ax_snr.plot(local_chunktimes, snrsq_fraction, '.-', label='acc. SNR$^2$')
        ax_snr.legend(loc='lower right')

        labstr=r"f$_{\rm thresh }$=%.2f"%(freqs_at_thresh[i])
        ax[i].axvline(0, color='r', label=labstr)
        ax[i].set_xlabel("Seconds from GPS {}".format(opts.trigtime))
        ax[i].set_ylabel(r"{} Whitened Strain ($\sigma$)".format(ifos[i]))
        ax[i].set_xlim(-0.1,0.1)
        ax[i].legend(loc='upper left')
        ax[i].minorticks_on()
        ax_snr.minorticks_on()


        ax_snr.set_ylabel("Accumulated network SNR$^2$")

    f.tight_layout()
    #plt.show()
    f.savefig("freqsnr.png")










