#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
median_waves.py

Dump out a file with the median waveform from BW samples

"""

import sys
import numpy as np

Nifos=2
for i in xrange(Nifos):
    data = np.loadtxt(sys.argv[1]+'.{}'.format(i))
    median_data = np.median(data, axis=0)
    times = np.arange(len(median_data))
    np.savetxt("signal_median_time_domain_waveform.dat.{}".format(i),
            np.array([times, median_data]).T)
