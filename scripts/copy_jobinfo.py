#/usr/bin/env python

import os
import shutil
import sys

target='C0X_signalOnly_CBCPE_run9-1167559930-1167559940'

input_dir = sys.argv[1]
current_dir = input_dir.split('/')[-1]
current_dir_time = current_dir.split('_')[1]

shutil.copy(os.path.join(input_dir,'job_info.txt'),
        os.path.join(target,'bayeswave_'+current_dir_time+'*'))
#print os.path.join(input_dir,'job_info.txt'), os.path.join(target,'bayeswave_'+current_dir_time+'*')

