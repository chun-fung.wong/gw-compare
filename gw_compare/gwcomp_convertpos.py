#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Converts LALInference hdf5 posterior samples files to text.

Native output for LALInference is now hdf5.  The gw-reconstruct module expects
the ascii format posterior_samples.dat usually produced by cbcBayesPostProc.py.
This script does the conversion
"""

import os,argparse
from lalinference import bayespputils as bppu

def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("infile", type=str, default="pos.hdf5", 
            help="Input HDF5 posterior samples file from LALInference")
    parser.add_argument("--output-file", type=str,
            default="posterior_samples.dat",
            help="Name for posterior samples output file")
    parser.add_argument("--outdir", type=str, default="./",
            help="Parent directory of posterior samples output")
    args = parser.parse_args()

    return args

def convert_samples(infile,outfile):
    """
    Load posterior samples into peparser object from bayespputils and write to
    ascii.
    """
    peparser = bppu.PEOutputParser('hdf5')
    commonResultsObj = peparser.parse(infile)
    pos = bppu.Posterior(commonResultsObj)
    pos.write_to_file(outfile)

    return 0

def main():

    args = parser()

    print "converting {0} to {1}".format(args.infile,
            os.path.join(args.outdir, args.output_file))

    convert_samples(args.infile,args.output_file)

    if os.path.isfile(os.path.join(args.outdir, args.output_file)):
        print "success"
    else:
        print "failure, {} does not exist".format(os.path.join(args.outdir,
            args.output_file))



    return 0

if __name__ == "__main__":
    main()



