#!/bin/sh -e

# Top level lalinference_o2 lalsuite installation (where etc lives):
LALSUITE=${HOME}/opt/lscsoft/lalsuite

# Destination for virtual environment:
VIRTUALENV=${HOME}/virtualenvs/gw-compare

# Location of gw-compare source directory (probably PWD for this file):
REPODIR=${HOME}/src/lscsoft/gw-compare

# Install:
virtualenv ${VIRTUALENV}
echo "source ${LALSUITE}/etc/lalsuiterc" >> ${VIRTUALENV}/bin/activate
source ${VIRTUALENV}/bin/activate
#pip install pip --upgrade
pip install setuptools==42.0.1
pip install git+file://${REPODIR} -r ${REPODIR}/requirements.txt
